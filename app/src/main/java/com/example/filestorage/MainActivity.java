package com.example.filestorage;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFileName = (EditText) findViewById(R.id.etFilename);
        mFileContent = (TextView) findViewById(R.id.tvFileContent)
    }

    public void onClick(View view) {
        String fileName = mFileName.getText().toString();
        Context context = view.getContext();
        if (TextUtils.isEmpty(fileName)) {
            showEmptyFilenameWarning(context);
            return;
        }
        switch (view.getId()) {
            case R.id.btnSave:
                saveToFile(context, fileName);
                break;

            case R.id.btnRead:
                readFromFile(context, fileName);
                break;

        }
    }

    private void readFromFile(Context context, String fileName) {
      try  {
          FileInputStream fis = context.openFileInput(fileName);
          StringBuilder stringBuffer = new StringBuilder();
          int readByte;
          while ((readByte = fis.read()) != -i){
             stringBuffer.append(char) readByte);
          }
          fis.close();
          mFileContent.setText(stringBuffer.toString());
      } catch (Exception ex) {
          ex.printStackTrace();
      }
    }

   private void saveToFile(Context context, String fileName) {
    try {
        FileOutputStream fos = context.openFileOutput(fileName, context.MODE_PRIVATE);
        for (int i = 0; i < NUM_COPIES; i++) {
             fos.write(fileName.toString()).getBytes());
        }
       fos.close();
    } catch (Exception ex){
       ex.printStackTrace();
    }
   }

  private void showEmptyFilenameWarning(Context context) {
      Toast.makeText(context, "Please enter a valid name",
              Toast.LENGTH_LONG).show();
  }
}




